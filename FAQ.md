# Frequently Asked Questions

## How do I use a self-signed certificate to enable Slicer Extensions on my network?

In the 3D Slicer Application it is possible to run a Python script at startup to set
environment variables.

In the Slicer menu choose `Edit->Application Settings`. In the "Settings" window, under
the "General" list item, you will see "Application startup script:". Clicking on the
green circle with the white arrow on the far right will open the startup script in an
editor on your operating system.

Paste the below code into the file, change `/path/to/my/certificate/file` to the full
path of your self-signed certificate, save the file, and restart 3D Slicer.

```python
# set REQUESTS_CA_BUNDLE, CURL_CA_BUNDLE, SSL_CERT_FILE
import os
Cert_Path = "/path/to/my/certificate/file"
os.environ['CURL_CA_BUNDLE'] = Cert_Path
os.environ['REQUESTS_CA_BUNDLE'] = Cert_Path
os.environ['SSL_CERT_FILE'] = Cert_Path
```
