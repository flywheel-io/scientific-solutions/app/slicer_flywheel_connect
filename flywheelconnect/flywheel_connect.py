"""Flywheel Connect - Connect to Flywheel and upload data to Slicer."""
import datetime
import importlib
import json
import logging
import platform
import shutil
import tempfile
from glob import glob
from pathlib import Path
from zipfile import ZipFile

import ctk
import DICOMLib
import qt
import slicer
import vtk
from slicer.ScriptedLoadableModule import (
    ScriptedLoadableModule,
    ScriptedLoadableModuleLogic,
    ScriptedLoadableModuleTest,
    ScriptedLoadableModuleWidget,
)

# fmt: off
# Check for and install missing requirements
source_dir = Path(__file__).parent
resource_dir = source_dir / "Resources"

FLYWHEEL_CONFIG_DIR = Path.home() / ".config" / "flywheel"
CONFIG_PATH = FLYWHEEL_CONFIG_DIR / "fw_connect.json"
CONFIG_DEFAULTS = json.load((resource_dir / "config_defaults.json").open())
REQUIRED_MODULES = json.load((resource_dir / "required_modules.json").open())
PAIRED_FILE_TYPES = json.load((resource_dir / "paired_file_types.json").open())

if not importlib.util.find_spec("fw_pyqt_management"):
    slicer.util.pip_install("fw-pyqt-management")
    
from fw_pyqt_management.utils import check_developer_mode, check_requirements

check_requirements(REQUIRED_MODULES)

import flywheel
from fw_pyqt_management.config import Config
from fw_pyqt_management.fw_container_items import (
    AnalysisItem,
    CollectionItem,
    ContainerItem,
    FileItem,
)
from fw_pyqt_management.tree_management import TreeManagement

# fmt: on

log = logging.getLogger(__name__)

#
# flywheel_connect
#


class flywheel_connect(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class.

    available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        """Constructor."""
        super().__init__(parent)
        self.parent.title = "Flywheel Connect"
        self.parent.categories = ["Flywheel"]
        self.parent.dependencies = []
        self.parent.contributors = ["Joshua Jacobs (flywheel.io)"]
        self.parent.helpText = (
            'See <a href="https://github.com/flywheel-apps/SlicerFlywheelConnect">'
            "Flywheel Connect website</a> for more information."
        )
        self.parent.helpText += self.getDefaultModuleDocumentationLink()
        self.parent.acknowledgementText = (
            "This has been a collaboration with flywheel.io"
        )


#
# flywheel_connectWidget
#


class flywheel_connectWidget(ScriptedLoadableModuleWidget):
    """Uses ScriptedLoadableModuleWidget base class.

    available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setup(self):
        """Initialize all form elements."""
        super().setup()
        check_developer_mode()
        self.paired_file_types = json.load(
            (resource_dir / "paired_file_types.json").open()
        )
        # Get config
        self.config = Config(CONFIG_PATH, CONFIG_DEFAULTS)

        # Declare Cache path
        if self.config.get("cache_dir"):
            self.cache_dir = Path(self.config.get("cache_dir"))
        else:
            self.cache_dir = (
                Path(self.config["CachePath"]) / self.config["CacheDirName"]
            )

        self.instance = ""
        self.fw_client = None
        self.source_dir = Path(__file__).parent
        self.icon_dir = self.source_dir / "Resources" / "Icons"
        # #################Declare form elements#######################

        # Give a line_edit and label for the API key
        self.apiKeyCollapsibleGroupBox = ctk.ctkCollapsibleGroupBox()
        self.apiKeyCollapsibleGroupBox.setTitle("API Key Entry")

        self.layout.addWidget(self.apiKeyCollapsibleGroupBox)
        apiKeyFormLayout = qt.QFormLayout(self.apiKeyCollapsibleGroupBox)

        #
        # api Key Text Box
        #
        self.apiKeyTextLabel = qt.QLabel("API Key:")
        apiKeyFormLayout.addWidget(self.apiKeyTextLabel)
        self.apiKeyTextBox = qt.QLineEdit()
        self.apiKeyTextBox.setEchoMode(qt.QLineEdit.Password)

        apiKeyFormLayout.addWidget(self.apiKeyTextBox)
        self.connectAPIButton = qt.QPushButton("Connect Flywheel")
        self.connectAPIButton.enabled = True
        apiKeyFormLayout.addWidget(self.connectAPIButton)

        self.logAlertTextLabel = qt.QLabel("")
        apiKeyFormLayout.addWidget(self.logAlertTextLabel)

        #
        # cacheDir Text Box
        #
        # TODO: Consider removing this entirely. Move to fixed temp directory structure
        self.cacheDirTextLabel = qt.QLabel("Disk Cache:")
        apiKeyFormLayout.addWidget(self.cacheDirTextLabel)
        self.cacheDirTextBox = qt.QLineEdit()
        self.cacheDirTextBox.setText(self.cache_dir)
        # Create a platform-dependent validator to
        # ensure that the cache directory is a valid path
        if platform.system() == "Windows":
            reg_ex = qt.QRegExp("^[a-zA-Z]:\\\\[\\\\\w\-.]+$")
        else:
            reg_ex = qt.QRegExp("^\/[\/\w\-.]+$")
        input_validator = qt.QRegExpValidator(reg_ex, self.cacheDirTextBox)
        self.cacheDirTextBox.setValidator(input_validator)
        apiKeyFormLayout.addWidget(self.cacheDirTextBox)

        # Use a Clear Cache Button:
        self.clear_cache_button = qt.QPushButton("Clear Cache")
        self.clear_cache_button.toolTip = "Clear the cache directory."
        self.clear_cache_button.enabled = True
        apiKeyFormLayout.addWidget(self.clear_cache_button)

        # Data View Section
        self.dataCollapsibleGroupBox = ctk.ctkCollapsibleGroupBox()
        self.dataCollapsibleGroupBox.setTitle("Data")
        self.layout.addWidget(self.dataCollapsibleGroupBox)

        dataFormLayout = qt.QFormLayout(self.dataCollapsibleGroupBox)

        #
        #  collection toggle button
        #

        self.radioButtonGroup = qt.QButtonGroup()
        self.useCollections = qt.QRadioButton("Browse Collections")
        self.useProjects = qt.QRadioButton("Browse Groups and Projects")
        self.radioButtonGroup.addButton(self.useCollections)
        self.radioButtonGroup.addButton(self.useProjects)
        self.useProjects.setChecked(True)
        self.useProjects.enabled = False
        self.useCollections.enabled = False
        dataFormLayout.addWidget(self.useProjects)
        dataFormLayout.addWidget(self.useCollections)

        #
        # group Selector ComboBox
        #
        self.groupSelectorLabel = qt.QLabel("Current group:")
        dataFormLayout.addWidget(self.groupSelectorLabel)

        # Selector ComboBox
        self.groupSelector = qt.QComboBox()
        self.groupSelector.enabled = False
        self.groupSelector.setMinimumWidth(200)
        dataFormLayout.addWidget(self.groupSelector)

        #
        # project Selector ComboBox
        #
        self.projectSelectorLabel = qt.QLabel("Current project:")
        dataFormLayout.addWidget(self.projectSelectorLabel)

        # Selector ComboBox
        self.projectSelector = qt.QComboBox()
        self.projectSelector.enabled = False
        self.projectSelector.setMinimumWidth(200)
        dataFormLayout.addWidget(self.projectSelector)

        #
        # collection Selector ComboBox
        #

        # Selector ComboBox
        self.collectionSelector = qt.QComboBox()
        self.collectionSelector.enabled = False
        self.collectionSelector.visible = False
        self.collectionSelector.setMinimumWidth(200)
        dataFormLayout.addWidget(self.collectionSelector)

        # TreeView for Single Projects containers:
        self.treeView = qt.QTreeView()

        self.treeView.enabled = False
        self.treeView.setMinimumWidth(200)
        self.treeView.setMinimumHeight(350)
        self.tree_management = TreeManagement(
            self.fw_client,
            self.treeView,
            self.cache_dir,
            paired_file_types=PAIRED_FILE_TYPES,
        )
        dataFormLayout.addWidget(self.treeView)

        # Load Files Button
        self.loadFilesButton = qt.QPushButton("Load Selected Files")
        self.loadFilesButton.enabled = False
        dataFormLayout.addWidget(self.loadFilesButton)

        # Upload to Flywheel Button
        self.uploadFilesButton = qt.QPushButton(
            "Upload to Flywheel\nas Container Files"
        )
        self.uploadFilesButton.enabled = False
        dataFormLayout.addWidget(self.uploadFilesButton)

        # As Analysis Checkbox
        self.asAnalysisCheck = qt.QCheckBox("As Analysis")
        self.asAnalysisCheck.toolTip = (
            "Upload Files to Flywheel as an Analysis Container."
        )
        self.asAnalysisCheck.enabled = False

        dataFormLayout.addWidget(self.asAnalysisCheck)

        # ################# Connect form elements #######################

        # Connect Cache Dir TextBox to Cache Dir
        self.cacheDirTextBox.connect(
            "textChanged(QString)", self.onCacheDirTextBoxChanged
        )

        self.connectAPIButton.connect("clicked(bool)", self.onConnectAPIPushed)

        self.clear_cache_button.connect("clicked(bool)", self.on_clear_cache_pushed)

        # self.useCollectionCheckBox.connect("clicked(bool)", self.onUseCollectionChecked)
        self.useCollections.connect("clicked(bool)", self.onProjectsOrCollections)
        self.useProjects.connect("clicked(bool)", self.onProjectsOrCollections)

        self.collectionSelector.connect(
            "currentIndexChanged(QString)", self.onCollectionSelected
        )

        self.groupSelector.connect("currentIndexChanged(QString)", self.onGroupSelected)

        self.projectSelector.connect(
            "currentIndexChanged(QString)", self.onProjectSelected
        )

        self.loadFilesButton.connect("clicked(bool)", self.onLoadFilesPushed)

        self.uploadFilesButton.connect("clicked(bool)", self.save_scene_to_flywheel)

        self.asAnalysisCheck.stateChanged.connect(self.onAnalysisCheckChanged)

        # Connect treeView selection to loadFilesButton
        self.treeView.selectionModel().selectionChanged.connect(
            self.on_selection_changed
        )

        # Add vertical spacer
        self.layout.addStretch(1)

    def onCacheDirTextBoxChanged(self, text):
        """Set Cache Dir.

        Args:
            text (str): Valid cache dir.
        """
        self.cache_dir = Path(text)
        # Set cache directory with instance
        self.tree_management.set_cache_dir(self.cache_dir / self.instance)
        self.config["cache_dir"] = text

    def get_cli_api_key(self):
        """Get the api key from a successful CLI loging to Flywheel.

        e.g. `fw login <api-key>`

        Which puts the api key in the config file at ~/.config/flywheel/user.json

        Returns:
            str: api key or None
        """
        user_json_path = Path.home() / ".config" / "flywheel" / "user.json"
        api_key = None
        if user_json_path.exists():
            with user_json_path.open() as f:
                user_json = json.load(f)
                api_key = user_json.get("key")

        return api_key

    def get_fw_client(self):
        """Retrieve the Flywheel Client.

        This function retrieves the Flywheel Client from various sources:
        - If the api-key is entered in the text box, this will be used.
        - Else, If an api-key is in the config file, this will be used.
        - Else, If an api-key is in the CLI-generated ~/.config/flywheel/user.json,
          this will be used.
        - Else, None is returned.

        Returns:
            str: The api-key or None
        """
        fw_client = None
        # Load values from config file. This will be a str or None.
        api_key = self.config.get("api_key")

        # Instantiate and connect widgets ...
        # if valid, the api key in the text box will overide the config contents
        if self.apiKeyTextBox.text:
            if api_key != self.apiKeyTextBox.text:
                # reset config file
                for key in ["group", "project"]:
                    if self.config.get(key):
                        del self.config[key]
                api_key = self.apiKeyTextBox.text
        elif not api_key:
            api_key = self.get_cli_api_key()

        if api_key:
            fw_client = flywheel.Client(api_key)
            self.config["api_key"] = api_key

        return fw_client

    def onConnectAPIPushed(self):
        """Connect to a Flywheel instance for valid api-key."""
        try:
            qt.QApplication.setOverrideCursor(qt.Qt.WaitCursor)

            self.fw_client = self.get_fw_client()

            if self.fw_client is None:
                raise Exception("Could not find a valid api-key. Please try again.")

            fw_user = self.fw_client.get_current_user()["email"]
            fw_site = self.fw_client.get_config()["site"]["api_url"]
            self.instance = fw_site.split("/")[2].split(":")[0]
            self.tree_management.set_fw_client(self.fw_client)
            self.tree_management.set_instance(self.instance)

            self.logAlertTextLabel.setText(
                f"You are logged in as {fw_user} to {fw_site}"
            )

            # Set cache directory with instance
            self.tree_management.set_cache_dir(self.cache_dir / self.instance)

            # Initialize dropdown menu with groups, projects, and collections
            self.init_dropdowns = True
            self.useProjects.enabled = True
            self.useCollections.enabled = True
            # initialize collections
            collections = self.fw_client.collections()
            self.collectionSelector.enabled = False
            self.collectionSelector.clear()
            for collection in collections:
                self.collectionSelector.addItem(collection.label, collection.id)

            # initialize collection from config
            if self.config.get("collection"):
                self.collectionSelector.setCurrentIndex(
                    self.collectionSelector.findText(self.config.get("collection"))
                )
                if self.useCollections.checked:
                    self.onCollectionSelected(self.config.get("collection"))

            self.useProjects.setChecked(True)

            # initialize groups and projects
            groups = self.fw_client.groups()
            self.groupSelector.enabled = True
            self.groupSelector.clear()
            for group in groups:
                self.groupSelector.addItem(group.label, group.id)

            # initialize group and project from config
            if self.config.get("group"):
                group_found = self.groupSelector.findText(self.config.get("group"))
                self.groupSelector.setCurrentIndex(group_found)
                if not self.useCollections.checked and group_found != -1:
                    self.onGroupSelected(self.config.get("group"))

            if self.config.get("project"):
                project_found = self.projectSelector.findText(
                    self.config.get("project")
                )
                self.projectSelector.setCurrentIndex(project_found)
                if not self.useCollections.checked and project_found != -1:
                    self.onProjectSelected(self.config.get("project"))

            self.init_dropdowns = False
            # Clear out any other instance's data from Slicer before proceeding.
            slicer.mrmlScene.Clear(0)
        except Exception as exp:
            log.exception(exp)
            self.groupSelector.clear()
            self.groupSelector.enabled = False
            self.apiKeyTextBox.clear()
            self.projectSelector.clear()
            self.projectSelector.enabled = False
            self.collectionSelector.clear()
            self.collectionSelector.enabled = False
            slicer.util.errorDisplay(exp)
        finally:
            qt.QApplication.restoreOverrideCursor()

    def on_clear_cache_pushed(self):
        """Clear the cache directory."""
        # TODO: Add a confirmation dialog?
        if self.tree_management.cache_dir.exists():
            shutil.rmtree(self.tree_management.cache_dir)
            self.cache_dir.mkdir(parents=True, exist_ok=True)
            self.tree_management.set_cache_dir(self.cache_dir / self.instance)

    def onProjectsOrCollections(self):
        """Toggle between browsing projects and collections."""
        tree_rows = self.tree_management.source_model.rowCount()
        if self.useCollections.checked:
            self.projectSelectorLabel.setText("Current collection:")
            self.projectSelector.enabled = False
            self.projectSelector.visible = False
            self.groupSelectorLabel.visible = False
            self.groupSelector.enabled = False
            self.groupSelector.visible = False
            self.collectionSelector.enabled = True
            self.collectionSelector.visible = True
            collection_id = self.collectionSelector.currentData
            if collection_id:
                self.collection = self.fw_client.get(collection_id)
                # Remove the rows from the tree and repopulate
                if tree_rows > 0:
                    self.tree_management.source_model.removeRows(0, tree_rows)
                self.tree_management.populate_tree_from_collection(self.collection)
                self.treeView.enabled = True
            else:
                self.treeView.enabled = False
        else:
            self.projectSelectorLabel.setText("Current project:")
            self.projectSelector.enabled = True
            self.projectSelector.visible = True
            self.groupSelectorLabel.visible = True
            self.groupSelector.enabled = True
            self.groupSelector.visible = True
            self.collectionSelector.enabled = False
            self.collectionSelector.visible = False
            project_id = self.projectSelector.currentData
            if project_id:
                self.project = self.fw_client.get(project_id)
                # Remove the rows from the tree and repopulate
                if tree_rows > 0:
                    self.tree_management.source_model.removeRows(0, tree_rows)
                self.tree_management.populate_tree_from_project(self.project)
                self.treeView.enabled = True
            else:
                self.treeView.enabled = False

    def onCollectionSelected(self, item):
        """On selected collection from dropdown, update the tree.

        Args:
            item (str): Name of collection or empty string
        """
        tree_rows = self.tree_management.source_model.rowCount()
        if item:
            collection_id = self.collectionSelector.currentData
            self.collection = self.fw_client.get(collection_id)
            if not self.init_dropdowns:
                self.config["collection"] = self.collectionSelector.currentText

            # Remove the rows from the tree and repopulate
            if tree_rows > 0:
                self.tree_management.source_model.removeRows(0, tree_rows)
            self.tree_management.populate_tree_from_collection(self.collection)
            self.treeView.enabled = True
        else:
            self.treeView.enabled = False
            # Remove the rows from the tree and don't repopulate
            if tree_rows > 0:
                self.tree_management.source_model.removeRows(0, tree_rows)
            self.loadFilesButton.enabled = False

    def onGroupSelected(self, item):
        """On selected Group from dropdown, update casecade.

        Args:
            item (str): Group name or empty string
        """
        if item:
            group_id = self.groupSelector.currentData
            self.group = self.fw_client.get(group_id)
            if not self.init_dropdowns:
                self.config["group"] = self.groupSelector.currentText
            projects = self.group.projects()
            self.projectSelector.enabled = len(projects) > 0
            self.projectSelector.clear()
            for project in projects:
                self.projectSelector.addItem(project.label, project.id)

    def onProjectSelected(self, item):
        """On selected project from dropdown, update the tree.

        Args:
            item (str): Name of project or empty string
        """
        tree_rows = self.tree_management.source_model.rowCount()
        if item:
            project_id = self.projectSelector.currentData
            self.project = self.fw_client.get(project_id)
            if not self.init_dropdowns:
                self.config["project"] = self.projectSelector.currentText

            # Remove the rows from the tree and repopulate
            if tree_rows > 0:
                self.tree_management.source_model.removeRows(0, tree_rows)
            self.tree_management.populate_tree_from_project(self.project)
            self.treeView.enabled = True
        else:
            self.treeView.enabled = False
            # Remove the rows from the tree and don't repopulate
            if tree_rows > 0:
                self.tree_management.source_model.removeRows(0, tree_rows)
            self.loadFilesButton.enabled = False

    def on_selection_changed(self):
        """Enable or disable load and upload buttons based on selected tree items.

        If a FileItem is selected, the load button is enabled.
        Else if a ContainerItem (e.g. Project, Session,...) is selected, upload is
        is enabled.
        """
        indexes = self.treeView.selectedIndexes()
        has_file = False
        containers_selected = 0
        analysis_valid = True
        if len(indexes) > 0:
            for index in indexes:
                item = self.tree_management.source_model.itemFromIndex(index)
                if isinstance(item, FileItem):
                    has_file = True
                # Analysis Containers cannot be altered.
                elif isinstance(item, AnalysisItem):
                    containers_selected = 2
                # Collection Containers have no Analyses.
                elif isinstance(item, CollectionItem):
                    containers_selected += 1
                    analysis_valid = False
                elif isinstance(item, ContainerItem):
                    containers_selected += 1
        else:
            has_file = False

        self.loadFilesButton.enabled = has_file
        upload_enabled = containers_selected == 1
        self.uploadFilesButton.enabled = upload_enabled
        self.asAnalysisCheck.enabled = analysis_valid and upload_enabled
        if self.asAnalysisCheck.isChecked():
            self.asAnalysisCheck.setChecked(analysis_valid).main_window

    def is_compressed_dicom(self, file_path, file_type):
        """Check file_path and file_type for a flywheel compressed dicom archive.

        Args:
            file_path (str): Path to cached file
            file_type (str): Type of Flywheel file

        Returns:
            boolean: True for supported compressed dicom type
        """
        if file_path.endswith(".zip") and file_type == "dicom":
            return True

        return False

    def is_uncompressed_dicom(self, file_path, file_type):
        """Check file_path and file_type for a flywheel uncompressed dicom directory.

        Args:
            file_path (str): Path to cached file
            file_type (str): Type of Flywheel file

        Returns:
            boolean: True for supported uncompressed dicom type
        """
        if file_path.lower().endswith((".dcm", ".dicom")) and file_type == "dicom":
            return True

        return False

    def load_dicom_file(self, file_path):
        """Load dicom file into Slicer.

        Args:
            file_path (str): path to the cached dicom file.

        https://discourse.slicer.org/t/fastest-way-to-load-dicom/9317/2
        """
        loadablesByPlugin, loadEnabled = DICOMLib.getLoadablesFromFileLists(
            [[file_path]]
        )
        loadedNodeIDs = DICOMLib.loadLoadables(loadablesByPlugin)

    def load_dicom_archive(self, file_path):
        """Load unzipped DICOMs into Slicer.

        Args:
            file_path (str): path to the cached dicom archive.

        https://discourse.slicer.org/t/fastest-way-to-load-dicom/9317/2
        """
        with tempfile.TemporaryDirectory() as dicomDataDir:
            dicom_zip = ZipFile(file_path)
            dicom_zip.extractall(path=dicomDataDir)
            DICOMLib.importDicom(dicomDataDir)
            dicomFiles = slicer.util.getFilesInDirectory(dicomDataDir)
            loadablesByPlugin, loadEnabled = DICOMLib.getLoadablesFromFileLists(
                [dicomFiles]
            )
            loadedNodeIDs = DICOMLib.loadLoadables(loadablesByPlugin)

    def load_slicer_file(self, file_path, file_type):
        """Load filepath based on type.

        Args:
            file_path (str): Path to file to load
            file_type (str): String representing of file type
        """
        # Check for Flywheel compressed dicom
        if self.is_compressed_dicom(file_path, file_type):
            try:
                self.load_dicom_archive(file_path)
                return True
            except Exception as exp:
                log.exception(exp)
                log.error("Not a valid DICOM archive.")
                return False
        elif self.is_uncompressed_dicom(file_path, file_type):
            try:
                self.load_dicom_file(file_path)
                return True
            except Exception as exp:
                log.exception(exp)
                log.error("Not a valid DICOM directory.")
                return False
        # Load using Slicer default node reader
        else:
            try:
                if slicer.app.ioManager().loadFile(file_path):
                    # Set the parent_container_id on the last loaded node
                    nodes = slicer.util.getNodesByClass("vtkMRMLStorageNode")
                    filtered_nodes = [
                        node
                        for node in nodes
                        if node.GetFileName()
                        and (Path(node.GetFileName()).name == Path(file_path).name)
                    ]
                    if not filtered_nodes and file_path.split(".")[-1] == "mrml":
                        log.debug("Loaded Slicer Scene.")
                        return True

                    node = filtered_nodes[-1]
                    if Path(node.GetFileName()).name == Path(file_path).name:
                        node.SetAttribute(
                            "parent_container_id", Path(file_path).parent.name
                        )
                    return True
                else:
                    log.error("Failed to read file: %s", file_path)
                    return False
            except Exception as exc:
                log.exception(exc)
                log.error("Failed to read file: %s", file_path)
                return False

    def onLoadFilesPushed(self):
        """Load tree-selected files into 3D Slicer for viewing."""
        try:
            qt.QApplication.setOverrideCursor(qt.Qt.WaitCursor)
            # Cache all selected files
            self.tree_management.cache_selected_for_open()

            # Walk through cached files... This could use "types"
            for _, file_dict in self.tree_management.cache_files.items():
                file_path = file_dict["file_path"]
                file_type = file_dict["file_type"]
                _ = self.load_slicer_file(file_path, file_type)
            # Set cache directory to refresh cache status in current tree
            self.tree_management.set_cache_dir(self.cache_dir / self.instance)
        except Exception as exc:
            log.exception(exc)
        finally:
            qt.QApplication.restoreOverrideCursor()

    def save_analysis(self, parent_container_item, output_path):
        """Save selected files to a new analysis container under a parent container.

        Args:
            parent_container_item (ContainerItem): Tree Item representation of parent
                container.
            output_path (Path): Temporary path to where Slicer files are saved.
        """
        parent_container = self.fw_client.get(parent_container_item.data())

        # Get all cached paths represented in Slicer
        input_files_paths = [
            Path(node.GetFileName())
            for node in slicer.util.getNodesByClass("vtkMRMLStorageNode")
            # Check if the file is in the cache directory
            if self.cache_dir._str in node.GetFileName() and
            # Ignore the current temp directory
            output_path._str not in node.GetFileName()
        ]

        # Represent those files as file reference from their respective parents
        input_files = [
            self.fw_client.get(str(input_path.parents[0]).split("/")[-1])
            .get_file(input_path.name)
            .ref()
            if input_path.is_symlink()
            else self.fw_client.get(str(input_path.parents[1]).split("/")[-1])
            .get_file(input_path.name)
            .ref()
            for input_path in input_files_paths
        ]

        # Generic name... could be improved.
        analysis_name = "3D Slicer " + datetime.datetime.now().strftime(
            "%Y-%m-%d %H:%M:%S"
        )

        # Create analysis container
        info = {"origin": {"type": "user", "id": self.fw_client.get_current_user().id}}
        analysis = parent_container.add_analysis(
            label=analysis_name, inputs=input_files, info=info
        )

        # Get all files from temp directory
        outputs = [
            file_path
            for file_path in glob(str(output_path / "*"))
            if Path(file_path).is_file()
        ]

        # Finalize analysis
        # TODO: Change with update to SDK
        # BUG: https://flywheelio.atlassian.net/browse/FLYW-20865
        fw_version = list(map(int, flywheel.flywheel.SDK_VERSION.split(".")))
        if fw_version[0] >= 17 and fw_version[1] >= 5:
            # 17.5.0 has signed parameter default to True
            analysis.upload_file(outputs, signed=False)
        else:
            # pre 17.5.0 has signed parameter default to False
            # and not accessible from analysis.upload_file
            analysis.upload_file(outputs)
        # Refresh Analyses Folder
        parent_container_item.refresh_analyses()

    def save_files_to_container(self, parent_container_item, output_path):
        """Save selected files to a parent Flywheel container.

        Files that already exist in the container are ignored.

        TODO: parameterize "overwrite" and connect to a checkbox.

        Args:
            parent_container_item (ContainerItem):  Tree Item representation of parent
                container.
            output_path (Path): Temporary path to where Slicer files are saved.
        """
        overwrite = True
        parent_container_item.container = parent_container_item.container.reload()
        parent_container = parent_container_item.container
        parent_container_files = [fl.name for fl in parent_container.files]
        uploaded_files = []
        failed_files = []
        for output_file in [
            file_path
            for file_path in glob(str(output_path / "*"))
            if (
                Path(file_path).is_file()
                and (Path(file_path).name not in parent_container_files or overwrite)
            )
        ]:
            try:
                parent_container.upload_file(output_file)
                uploaded_files.append(output_file)
            except Exception as exc:
                failed_files.append(output_file)

                log.exception(exc)
                msg = f"The file, {output_file}, was not uploaded to Flywheel."
                log.error(msg)

        parent_container_item.refresh_files()
        self.tree_management.cache_uploaded_files(parent_container_item, uploaded_files)
        # TODO: Expand FILES folder to show newly uploaded files.
        # TODO: Do I want to update the mrmlScene.SetRootDirectory here?
        #       This would prevent the popup asking if you want to save the scene when
        #       you close Slicer.
        msg = ""
        if uploaded_files:
            msg += (
                f"The following files were uploaded to the {parent_container.label}: "
                f" {', '.join(uploaded_files)}.\n"
            )
        if failed_files:
            msg += (
                "The following files failed to upload to the "
                f"{parent_container.label}: "
                f" {', '.join(failed_files)}"
            )

        log.info(msg)

    def save_scene_to_flywheel(self):
        """Save selected files in the Slicer scene to a Flywheel Analysis or Container."""
        try:
            save_as_analysis = self.asAnalysisCheck.isChecked()
            # backup all filepaths from their original values
            for node in slicer.util.getNodesByClass("vtkMRMLStorageNode"):
                node.SetAttribute("original_file_path", node.GetFileName())

            # Get the first selected container item and its path
            index = self.treeView.selectedIndexes()[0]
            container_item = self.tree_management.source_model.itemFromIndex(index)
            # create a temp directory that is relative to all input files in the scene
            container_output_path = container_item._get_cache_path()
            container_output_path.mkdir(parents=True, exist_ok=True)
            if save_as_analysis:
                container_item_id = None
            else:
                container_output_path = container_output_path.parent
                container_item_id = container_item.container.id

            # Add trailing slash to path
            container_output_path = str(container_output_path / "_")[:-1]

            with tempfile.TemporaryDirectory(
                prefix=container_output_path
            ) as tmp_output_path:
                output_path = Path(tmp_output_path)
                slicer.mrmlScene.SetRootDirectory(str(output_path))
                slicer.mrmlScene.SetURL(str(output_path / "Slicer_Scene.mrml"))

                # Create list of all nodes that contain modified bulk data (stored in files)
                modifiedStorableNodes = vtk.vtkCollection()
                slicer.mrmlScene.GetStorableNodesModifiedSinceRead(
                    modifiedStorableNodes
                )
                modified_storage_nodes = [
                    node.GetStorageNode() for node in modifiedStorableNodes
                ]

                for node in slicer.util.getNodesByClass("vtkMRMLStorageNode"):
                    # If the node has a file on disk
                    if node.GetFileName():
                        # If the node has no parent container, give it one
                        if not node.GetAttribute("parent_container_id"):
                            node_path = Path(node.GetFileName())
                            parent_container_id = str(node_path.parents[1].name)
                            node.SetAttribute(
                                "parent_container_id", parent_container_id
                            )
                        if (
                            # If the node's parent container is the current container
                            container_item_id
                            == node.GetAttribute("parent_container_id")
                            or
                            # If the node is modified, it is
                            node in modified_storage_nodes
                        ):
                            # Save to the output directory
                            node.SetFileName(Path(node.GetFileName()).name)
                    else:
                        ns = slicer.util.getNodesByClass("vtkMRMLScalarVolumeNode")
                        node_id = node.GetID()
                        file_name = [
                            n.GetName()
                            for n in ns
                            if n.GetNodeReferenceID("storage") == node_id
                        ][0]
                        node.SetFileName(file_name)

                if slicer.util.openSaveDataDialog():
                    qt.QApplication.setOverrideCursor(qt.Qt.WaitCursor)
                    if save_as_analysis:
                        self.save_analysis(container_item, output_path)
                    else:
                        self.save_files_to_container(container_item, output_path)

                # Restore all filepaths to their original values
                for node in slicer.util.getNodesByClass("vtkMRMLStorageNode"):
                    original_file_path = node.GetAttribute("original_file_path")
                    if original_file_path:
                        node.SetFileName(original_file_path)
                        node.RemoveAttribute("original_file_path")

                # Remove storage nodes with the tmp_output_path in them
                for node in [
                    node
                    for node in slicer.util.getNodesByClass("vtkMRMLStorageNode")
                    if tmp_output_path in node.GetFileName()
                ]:
                    slicer.mrmlScene.RemoveNode(node)
        except Exception as exc:
            log.exception(exc)
        finally:
            qt.QApplication.restoreOverrideCursor()

    def onAnalysisCheckChanged(self, item):
        """Update the text on the "Upload" button depending on item state.

        Args:
            item (ItemData): Data from item... not used.
        """
        if self.asAnalysisCheck.isChecked():
            text = "Upload to Flywheel\nas Analysis"
        else:
            text = "Upload to Flywheel\nas Container Files"
        self.uploadFilesButton.setText(text)

    def cleanup(self):
        """Clean up the widget."""
        pass


#
# flywheel_connectLogic
#


class flywheel_connectLogic(ScriptedLoadableModuleLogic):
    """This class should implement all the actual computation done by your module.

    The interface should be such that other python code can import
    this class and make use of the functionality without
    requiring an instance of the Widget.
    Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def run(self):
        """Run the actual algorithm."""
        return True


class flywheel_connectTest(ScriptedLoadableModuleTest):
    """This is the test case for your scripted module.

    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        """Typically a scene clear will be enough."""
        slicer.mrmlScene.Clear(0)
        log.setLevel(logging.DEBUG)
        self.flywheel_connect_widget = slicer.modules.flywheel_connectWidget
        self.root_path = Path(__file__).parents[0]

    def cleanUp(self):
        """Clean up after tests."""
        slicer.mrmlScene.Clear(0)

    def runTest(self):
        """Run as few or as many tests as needed here."""
        log.info("Tests initiated.")
        self.setUp()
        self.test_load_nifti()
        self.test_load_compressed_dicom()
        self.test_load_metaimage()
        self.test_load_analyze()
        self.test_load_NRRD()
        self.cleanUp()
        log.info("Tests completed.")

    def test_load_nifti(self):
        """Test loading a nifti file."""
        log.info("Test loading NIFTI files.")
        nifti_path = self.root_path / "Testing/data/T1w_MPR.nii.gz"
        nifti_type = "nifti"
        succeeded = self.flywheel_connect_widget.load_slicer_file(
            str(nifti_path), nifti_type
        )
        assert succeeded

        log.info("Test of NIFTI Load Complete!")

    def test_load_compressed_dicom(self):
        """Test loading a compressed dicom file."""
        log.info("Testing loading compressed dicoms.")
        log.info("Load labeled compressed dicom.zip file.")
        file_path = str(self.root_path / "Testing/data/T1w_MPR.zip")
        file_type = "dicom"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert succeeded
        file_type = "not dicom"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert not succeeded
        log.info("Succeeded loading compressed dicoms.")

    def test_load_metaimage(self):
        """Test loading a metaimage file."""
        log.info("Testing load of metaimage files.")
        log.info("Load the header with the .raw file present.")
        file_path = str(
            self.root_path / "Testing/data/MFJK1C1F2_20200824_151907_S7to10_FWI_Fat.mhd"
        )
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert succeeded

        log.info("Load a header without the .raw file present")
        file_path = str(
            self.root_path
            / "Testing/data/MFJK1C1F2_20200824_151907_S7to10_FWI_FatPct.mhd"
        )
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert not succeeded

        log.info("attempt to load the .raw file")
        file_path = str(
            self.root_path / "Testing/data/MFJK1C1F2_20200824_151907_S7to10_FWI_Fat.raw"
        )
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert not succeeded
        log.info("Tests of metaimage files complete.")

    def test_load_analyze(self):
        """Test loading an analyze file."""
        log.info("Testing load of Analyze files.")
        log.info("Load the header with the .img file present")
        file_path = str(self.root_path / "Testing/data/T1w_MPR.hdr")
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert succeeded

        log.info("Load the .img without the header")
        file_path = str(self.root_path / "Testing/data/T1w_MPR.img")
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert succeeded

        log.info("Load a .hdr without .img file")
        file_path = str(self.root_path / "Testing/data/T1w_MPR3.hdr")
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert not succeeded
        log.info("Tests of Analyze files complete.")

    def test_load_NRRD(self):
        """Test loading an NRRD file."""
        log.info("Testing load of NRRD files.")
        log.info("Load standalone .nrrd file")
        file_path = str(self.root_path / "Testing/data/27 T1w_MPR.nrrd")
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert succeeded

        log.info("Load the .nhdr with data (.raw.gz) present")
        file_path = str(self.root_path / "Testing/data/27 T1w_MPR.nhdr")
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert succeeded

        log.info("Load a .raw.gz without .nhdr file")
        file_path = str(self.root_path / "Testing/data/27 T1w_MPR.raw.gz")
        file_type = "file"
        succeeded = self.flywheel_connect_widget.load_slicer_file(file_path, file_type)
        assert not succeeded
        log.info("Tests of NRRD files complete.")
