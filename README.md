# flywheel-connect <!-- omit in toc -->

A 3D Slicer Extension to view, select, and download images from a Flywheel instance to
[3D Slicer](https://www.slicer.org/) and storing Slicer outputs back to Flywheel.

Flywheel is a comprehensive data management solutions for life sciences and imaging
research.
More details at [https://flywheel.io/](https://flywheel.io/).

The Flywheel Connect 3D Slicer Extension is now in the [3D Slicer Extension
Catalog](https://extensions.slicer.org/view/flywheel_connect/30117/linux) and available
through the
[3D Slicer Extensions Manager](https://slicer.readthedocs.io/en/latest/user_guide/extensions_manager.html)
for direct installation.

- [Usage Instructions](#usage-instructions)
  - [Supported File Types](#supported-file-types)
    - [Tested File Types](#tested-file-types)
    - [Experimental File Types](#experimental-file-types)
- [System Requirements](#system-requirements)
  - [Windows Requirements](#windows-requirements)
  - [Enabling Symbolic Links on Windows 10/11](#enabling-symbolic-links-on-windows-1011)
- [File Management](#file-management)
  - [Directory Structure](#directory-structure)
- [Interface Overview](#interface-overview)
- [Frequently Asked Questions (FAQ)](#frequently-asked-questions-faq)

## Usage Instructions

1. Install 3D Slicer and the Flywheel extension as described [here](INSTALLATION.md).
2. Navigate to flywheel-connect under the Modules drop-down box.
    ![Select Extension](./Images/SelectExtension.png)

3. To access the Flywheel container hierarchy first enter your [Flywheel
api-key](https://docs.flywheel.io/hc/en-us/articles/360008162214) and press "connect".
If you have logged in with the Flywheel CLI (Command-Line-Interface), the cached
Flywheel api-key can be used without entering a new one.
4. Choose to "Browse Groups and Projects" or to "Browse Collections". See
[Flywheel Documentation](https://docs.flywheel.io/hc/en-us/articles/360007561074-Creating-and-Using-Collections)
for information about collections.
5. Navigate to the desired Acquisition in the Container Hierarchy Tree for a particular
project.
6. By clicking on a file or files in the "Files" of a container, they can be loaded
into 3D Slicer if they are [currently supported](#supported-file-types).
7. By clicking on a container, Slicer-derived files can be uploaded to Flywheel as

    - Files under a selected container ("Upload to Flywheel as Container Files")
      - All selected files (✓) in the Slicer save dialogue will be uploaded to the
        selected container
      - If a file exists with the same name it will be updated to the saved version
      - Slicer Scene files (.mrml) can be saved to a the selected container. See
        [Experimental Files Types](#experimental-file-types) below.
    - Files for a new Analysis object under a selected container ("Upload to Flywheel
      as Analysis Files").

### Supported File Types

Flywheel Connect can load most images or models of the
[3D Slicer Supported Data Formats](https://www.slicer.org/wiki/Documentation/4.8/SlicerApplication/SupportedDataFormat).
Compressed archives of DICOMs are also supported, if they are labeled as such in
Flywheel.

#### Tested File Types

- NIFTIs (.nii,.nii.gz)
- DICOMs (.dcm, .dicom, .dcm.zip, .zip (labeled as `dicom` type in Flywheel))
- MetaImage (.mhd/.raw, .mhd/.zraw)
- Analyze (.hdr/.img)
- NRRD (.nrrd)
  
(* "{header}/{data}" indicates "Paired" file type that represents coupled data.)

#### Experimental File Types

Slicer Scene files, [Medical Reality Modeling Language
(MRML)](https://slicer.readthedocs.io/en/latest/developer_guide/mrml_overview.html) can
be saved and loaded with the following condition:

- All referenced dependencies must be in the same container as the .mrml file

## System Requirements

Flywheel connect has been tested with 3D Slicer 4.11 and above.

### Windows Requirements

Flywheel Connect will run without additional requirements on Windows 10/11. However,
the default links will be "Hard Links".

To enable "Symbolic Links" on Windows 10/11, the user must run 3D Slicer as an
Administrator or enable Developer Mode.

### Enabling Symbolic Links on Windows 10/11

To enable Flywheel Connect to use symbolic links on Windows 10 or above you need to
either have
[Developer Mode](https://learn.microsoft.com/en-us/windows/apps/get-started/enable-your-device-for-development)
enabled or to run 3D Slicer
[As Administrator](https://www.computerhope.com/issues/ch001197.htm#:~:text=To%20run%20a%20program%20as%20Administrator%20in%20Windows%2010%2C%20right,and%20select%20Run%20as%20administrator.).

Additionally, the Windows 10/11 [Group Policy Editor](https://answers.microsoft.com/en-us/windows/forum/all/how-to-enable-the-gpeditmsc-on-windows-10-and-11/dbc76919-f2b5-4dec-a2b7-bcf545c34d00)
may need to be configured to [allow permissions for symbolic links](https://learn.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/create-symbolic-links).

This is to support the directory structure with symbolic links to files
([see below](#directory-structure)).

## File Management

Files will be cached to the flywheelIO/ directory of the users home directory.  This is
default and can be changed. If caching files is not desired, uncheck "Cache Images".  
This will delete all files in the cache between downloads.

### Directory Structure

Files in the cache directory are organized by
`{cache_dir}/{group_id}/{project_id}/{session_id}/{acquisition_id}/{file_id}/{file_name}`.

The latest version of each file will be stored in the `{file_id}` directory (see
[file versioning](https://docs.flywheel.io/hc/en-us/articles/360061207134-File-versioning))
with a symbolic link to the file at
`.../{acquisition_id}/{symlink to file_id/file_name}`.

Although Flywheel Connect can store multiple versions of a file it can only open the
most recent version.

## Interface Overview

The interface is shown below. Notable areas are commented on:

![Tree View](./Images/TreeD_Slicer.png)

- A) The API-Key can be entered here. Or the cached API-Key will be used from a
     previous `fw login {API-KEY}` command. When connecting to a Flywheel instance all data
     will be cleared from 3D Slicer to prevent invalid data references between Flywheel
     instances. If there is no previously cached login--or the supplied API-Key is
     invalid--an error dialog is displayed.
- B) Default disk cache.
- C) If unchecked, the cache will be cleared between downloads.
- D) Select Box for Groups. This will "cascade" selections for the first project, if it
     exists.
- E) Select Box for Projects. The selected project will clear and repopulate the tree.
     If no project exists, the tree is not enabled.
- F) Analyses objects are not automatically cached. Double-clicking will load all
     Analysis.
- G) Files that are cached will have a green "badge". Right-clicking on selected files
     will enable them to be cached. Some downloads are large.
- H) Load all selected files. Files that are Slicer-supported data formats (Images and
     Models) will be loaded. This will only be enabled if files are selected.
- I) Upload derived files to Flywheel Analysis or Container files. This will only be
     enabled if a single valid Flywheel Container is selected.
- H) If checked, indicates that derived files should be uploaded to Flywheel as
     Analysis output under the selected Container.

## Frequently Asked Questions (FAQ)

For answers to Frequently Asked Questions, see the [FAQ document](./FAQ.md).
