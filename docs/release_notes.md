# Release notes

Slicer extensions are released by git tags and date rather than semantic versioning.

## e67e5d9 (2023-07-27)

This release addresses errors related to changes in the `flywheel-sdk` package.

__Fixes__:

- Accounts for the `fw_client.get_current_user()` function no longer returning the
  `api_key` key in the dictionary.
- If the `api_key` is entered in the Extension Text Box, this is used.
- Else, if the `api_key` is present in the Extension Configuration, this is used.
- Else, if the `fw` CLI tool has successfully logged in, the `api_key` in the
  `user.json` file is used.
- Else, if the user is notified that they need a valid api key to log in.

__Documentation__:

- Add `docs`folder
- Add `docs/release_notes.md`
