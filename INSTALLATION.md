# Installation instructions

1. [Download](https://download.slicer.org/) and install 3D Slicer
2. Start 3D Slicer and open Extensions manager (menu: View / Extensions Manager)
3. Go to Install Extensions tab, and click Install button on Flywheel tile
4. Restart the application

## Install by File

For offline installation, download the extension from the
[extension catalog](https://slicer-packages.kitware.com/#search/results?query=flywheel&mode=text)
and install it manually.

If the extension is not available in the extension catalog, you can download the
extension package from the
[releases page](https://drive.google.com/drive/u/5/folders/17jtAXuawVQ8lE-QG8llhJmbMDr0NBBjT).
Choose the one that matches your operating system and architecture:

    - `31734-linux-amd64-flywheel_connect-gitbe568e4-2023-05-01.tar.gz`    

1. Open 3D Slicer
2. Open the Extension Manager
3. Choose "Install from file..."
4. Browse to the downloaded extension package and "Open"
5. Restart 3D Slicer

## For developers

Follow these steps to run a local copy of the extension:

1. Clone this github repository into a convenient location
2. Add the FlywheelConnect subfolder of the repository as an additional module path (in
menu: Edit / Application settings -> Modules -> Additional module paths; either
drag-and-drop the FlywheelConnect folder to the path list, or click the double-arrow
button and then click Add and select the FlywheelConnect folder)
3. Restart the application

## Bonus

In place of (2) above, install the DeveloperToolsForExtensions Extension and locate the
FlywheelConnect folder in the Extension Wizard.
