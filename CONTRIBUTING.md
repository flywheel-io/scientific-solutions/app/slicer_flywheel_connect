# Contributing

## Development Notes

flywheel-connect is built in the
[3D Slicer Extension Framework](https://www.slicer.org/wiki/Documentation/Nightly/Developers/Tutorials/Extension)
leveraging the [flywheel python SDK](https://pypi.org/project/flywheel-sdk/).
It currently allows an authenticated user to browse through a simple container hierarchy
(Groups->Projects->Sessions->Acquisitions) and valid retrieve files from a selected
acquisition to display in 3D Slicer. Future iterations of this extension are intended to

1. Access files, information, and analyses at all levels of the
Project->...->Acquisition hierarchy.
2. Download a Medical Record Bundle or an mrml file from flywheel and reinstantiate in
3D Slicer with all dependencies...referenced or contained.
    - Working for dependencies within the same container as the mrml/mrb file.

For developing with 3D Slicer, these 3D Slicer extensions (available through the
"Extensions Manager") are highly recommended:

- [DeveloperToolsForExtensions](https://www.slicer.org/w/index.php/Documentation/Nightly/Extensions/DeveloperToolsForExtensions)
- [DebuggingTools](https://www.slicer.org/w/index.php/Documentation/Nightly/Extensions/DebuggingTools)

Furthermore, the [3D Slicer discourse community](https://discourse.slicer.org/) has
been an invaluable resource.

## Releasing Updates to flywheel-connect

The 3D Slicer Extension Index automatically updates the extension on updates to the
`main` branch. To release a new version of flywheel-connect, simply merge your changes
from `develop` into `main`. The Extension Index will automatically update the extension
within 24 hours.
